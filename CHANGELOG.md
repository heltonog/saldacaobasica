# Changelog
O registro de todas as mundanças deste projeto vai ser documentada neste arquivo.

O formato está baseado no [Keep a Changelog](https://keepachangelog.com/pt-BR/1.0.0/),
e este projeto segue o [Versionamento Semântico](https://semver.org/lang/pt-BR/spec/v2.0.0.html).

## [Não publicado]

## [0.0.5] - 2020-04-20
### Adicionado
- Adicionando a tarefa "build" ao CI no arquivo ".gitlab-ci.yml".
- Criando o arquivo "src/main/resources/deb/bin/DEBIAN/control".
- Criando a meta "pacote" no arquivo "Makefile", para geração de pacote debian.
- Adicionando a tarefa "package" ao CI no arquivo ".gitlab-ci.yml".
- Adicionando a tarefa "deploy" ao CI no arquivo ".gitlab-ci.yml".
- Incluindo o diretório "releases" no arquivo ".gitignore".

## Modificado
- Colocando uma quebra de linha na mensagem da função principal em "src/main/c/saldacaobasica.c".

## [0.0.4] - 2020-04-20
### Adicionado
- Criando os arquivos ".gitlab-ci.yml" e "public/index.html".
- Criando a função principal no arquivo "src/main/c/saldacaobasica.c".
- Criando o arquivo de teste em "src/test/c/teste_saldacaobasica.c".
- Criando a meta "teste" no arquivo "Makefile", para testes unitários.
- Criando a meta "limpar" no arquivo "Makefile", para limpar os arquivos gerados.
- Incluindo o diretório "target" no arquivo ".gitignore".
- Criando a meta "compile" no arquivo "Makefile", para gerar o binário do programa do projeto.

## [0.0.3] - 2020-04-20
### Adicionado
- Criando o arquivo "src/main/c/saldacaobasica.c".

## [0.0.2] - 2020-04-20
### Adicionado
- Criando os arquivos "Makefile", ".gitignore" e "LeiaMeGit.txt".

## [0.0.1] - 2020-04-20
### Adicionado
- Criando este "CHANGELOG.md".
- Criando o arquivo "README.md".
