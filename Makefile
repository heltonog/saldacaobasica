# Nome do projeto.
PROJETO=saldacaobasica
# Versão do projeto.
VERSAO_PROJETO=0.0.5
# Código principal do projeto
PROGRAMA=saldacaobasica

# Diretório de código fonte.
SRC=src
# Diretório de arquivos alvos.
TARGET=target

####
# Compilando o código fonte
compile:
	# Diretórios alvo
	mkdir -vp ./${TARGET}/usr/bin
	# Gerando o binário
	gcc -std=c99 -o ./${TARGET}/usr/bin/${PROGRAMA} ./${SRC}/main/c/${PROGRAMA}.c

# Testes unitários
teste:
	# Criando diretórios arquivos alvos.
	mkdir -vp ./${TARGET}/usr/share/${PROJETO}/test
	# Gerando o binário do teste
	gcc -std=c99 -o ./${TARGET}/usr/share/${PROJETO}/test/teste_saldacaobasica ./${SRC}/test/c/teste_saldacaobasica.c -lcmocka
	# Executando o teste
	./${TARGET}/usr/share/${PROJETO}/test/teste_saldacaobasica

###
# Gerando o pacote debian
pacote: limpar compile
	# Criando diretórios de arquivos alvos.
	mkdir -vp ./${TARGET}/opt/${PROJETO}_${VERSAO_PROJETO}-all/DEBIAN
	mkdir -vp ./${TARGET}/opt/${PROJETO}_${VERSAO_PROJETO}-all/usr/bin
	
	# Criando arquivos necessário para o pacote debian
	cp -rf ./${SRC}/main/resources/deb/bin/DEBIAN/control ./${TARGET}/opt/${PROJETO}_${VERSAO_PROJETO}-all/DEBIAN/control
	
	# Nome do pacote
	echo "Package: ${PROJETO}" >> ./${TARGET}/opt/${PROJETO}_${VERSAO_PROJETO}-all/DEBIAN/control
	# Versão do pacote
	echo "Version: ${VERSAO_PROJETO}" >> ./${TARGET}/opt/${PROJETO}_${VERSAO_PROJETO}-all/DEBIAN/control
	# Pulando uma linha ao final
	echo >> ./${TARGET}/opt/${PROJETO}_${VERSAO_PROJETO}-all/DEBIAN/control
	
	# Copiando os arquivos que farão parte do pacote
	cp -rfv ./${TARGET}/usr/bin/${PROGRAMA} ./${TARGET}/opt/${PROJETO}_${VERSAO_PROJETO}-all/usr/bin/${PROGRAMA}
	
	# Gerando o pacote
	dpkg-deb --build ./${TARGET}/opt/${PROJETO}_${VERSAO_PROJETO}-all
	
	# Criando o diretórios de lançamento
	mkdir -vp ./public
	
	# Copiando o pacote para o diretório de lançamento
	cp -rfv ./${TARGET}/opt/${PROJETO}_${VERSAO_PROJETO}-all.deb ./public/${PROJETO}_${VERSAO_PROJETO}-all.deb

####
# Excluindo os arquivos resultantes.
limpar:
	# Excluindo o diretório de arquivos alvos.
	rm -rf ./${TARGET}

