// Bibliotecas necessárias para uso do teste "cmocka".
#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <stdint.h>
#include <cmocka.h>

// Exemplo de uso de teste.
static void teste_simples(void **state){
	assert_int_equal(1, 1);
	assert_int_equal(0, 0);
}

// Função principal.
int main(void){
	//
	
	// Vetor de testes.
	const struct CMUnitTest testes[] = {
		cmocka_unit_test(teste_simples), // O exemplo de uso de teste.
	};
	
	// Executando os testes do vetor.
	return cmocka_run_group_tests(testes, NULL, NULL);
}
